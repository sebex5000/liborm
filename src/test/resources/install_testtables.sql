use dbtest;
drop table if EXISTS LIBORM_PERSON;
drop table if exists LIBORM_PLZ;

CREATE TABLE LIBORM_PERSON
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    vorname VARCHAR(50),
    nachname VARCHAR(50),
    geschlecht VARCHAR(2),
    geb_datum DATE,
    plz_id INT
);
CREATE UNIQUE INDEX LIBORM_PERSON_ID_UINDEX ON LIBORM_PERSON (id);

CREATE TABLE LIBORM_PLZ
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ort VARCHAR(50),
    beschreibung TEXT
);
CREATE UNIQUE INDEX LIBORM_PLZ_ID_UINDEX ON LIBORM_PLZ (id);


insert into LIBORM_PLZ(id, ort) values (61250, 'Usingen');
insert into LIBORM_PLZ(id, ort) values (61440, 'Oberursel');
insert into LIBORM_PLZ(id, ort) values (60385, 'Frankfurt - Bornheim');

insert into LIBORM_PERSON (vorname, nachname, geschlecht, geb_datum, plz_id) values ('Sebastian', 'Lenz', 'm', date '1979-10-25', 61250);
insert into LIBORM_PERSON (vorname, nachname, geschlecht, geb_datum, plz_id) values ('Testuser1', 'abc', 'w', null, 61250);
insert into LIBORM_PERSON (vorname, nachname, geschlecht, geb_datum, plz_id) values ('Testuser2', 'defg', 'w', null, 61440);
COMMIT;

