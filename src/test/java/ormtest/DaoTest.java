package ormtest;

import org.junit.Assert;
import org.junit.Test;
import ormtest.model.generated.dao.LibormPerson;

import java.util.Arrays;

/**
 * Created by al13555 on 21.04.2017.
 */
public class DaoTest {

  @Test
  public void colTest() {
    Assert.assertEquals("Anzahl der Spalten in Liborm_person: ", 6, LibormPerson.COLS.length);
    Assert.assertEquals("Anzahl der Schlüssel in Liborm_person: ", 1, LibormPerson.KEYCOLS.length);
    Assert.assertEquals("Name der Tabelle liborm_person: ", "LIBORM_PERSON", LibormPerson.NAME);
    Assert.assertTrue("Column Vorname exists. ", Arrays.asList(LibormPerson.COLS).contains("vorname"));
  }
}
