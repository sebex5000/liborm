package ormtest.helper;



import de.sebex.orm.dbconnection.DBConnectionFactory;
import de.sebex.orm.dbconnection.DSConnectionParams;
import de.sebex.orm.dbconnection.mysql.MySqlConnectionFactory;
import de.sebex.orm.dbconnection.oracle.OracleConnectionFactory;

import java.sql.Connection;

/**
 * Created by al13555 on 21.04.2017.
 */
public class TestConfig {

  private static DBConnectionFactory connectionFactory = null;

  static {
    if (connectionFactory == null) {

      if (System.getProperty("dbtype").toUpperCase().equals("ORACLE")) {
        DSConnectionParams cp = new DSConnectionParams();
        cp.dbUrl = System.getProperty("dburl");
        cp.schema = System.getProperty("dbuser");
        cp.user = System.getProperty("dbuser");
        cp.password = System.getProperty("dbpassword");

        connectionFactory = new OracleConnectionFactory(cp);
      }
      else {
        DSConnectionParams cp = new DSConnectionParams();
        cp.dbUrl = System.getProperty("dburl")+":"+System.getProperty("dbport");
        cp.schema = System.getProperty("dbschema");
        cp.user = System.getProperty("dbuser");
        cp.password = System.getProperty("dbpassword");
        cp.schema= System.getProperty("dbschema");

        connectionFactory = new MySqlConnectionFactory(cp);
      }
    }
  }

  public static DBConnectionFactory getConnectionFactory() {
    return connectionFactory;
  }
}
