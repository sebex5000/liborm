package ormtest;

import de.sebex.orm.Criteria;
import de.sebex.orm.CriteriaBuilder;
import de.sebex.orm.dbconnection.DBConnectionFactory;
import org.junit.Assert;
import org.junit.Test;
import ormtest.helper.TestConfig;
import ormtest.model.generated.dao.LibormPerson;
import ormtest.model.generated.dao.LibormPlz;
import ormtest.model.generated.map.LibormPersonMapper;
import ormtest.model.generated.map.LibormPlzMapper;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by al13555 on 20.04.2017.
 */
public class SelectTest {


  @Test
  public void testSelectStream() {
    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {

      LibormPersonMapper mapper = new LibormPersonMapper();
      mapper.setConnection(connection);

      long count = mapper.selectByCriteriasStream(LibormPerson.class, LibormPerson.NAME, null).count();
      Assert.assertEquals("Falsche Anzahl von Zeilen in libOrmTesttable: " , 3, count);


    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testSelectQuery() {
    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {

      LibormPersonMapper mapper = new LibormPersonMapper();
      mapper.setConnection(connection);

      //Select �ber Standardmethode
      List<LibormPerson> list = mapper.selectByQuery(LibormPerson.class, "select * from LIBORM_PERSON" );
      Assert.assertEquals("Falsche Anzahl von Zeilen in libOrmTesttable: " , 3, list.size());
      Assert.assertEquals("Vorname von Satz-Index 1: ", "Testuser1", list.get(1).getVorname());
      for(LibormPerson libormPerson : list) {
        if (libormPerson.getId().equals(BigDecimal.valueOf(2))) {
          Assert.assertEquals("Vorname von Satz-ID 2: ", "Testuser1", libormPerson.getVorname());
        }
      }

      //Select �ber Streammethode
      Stream<LibormPerson> libormPersonStream = mapper.selectByQueryAsStream(LibormPerson.class, "select * from LIBORM_PERSON");


      Optional<LibormPerson> optionalperson = mapper.selectByQueryAsStream(LibormPerson.class, "select * from LIBORM_PERSON")
        .filter(
                person -> person.getId()==2
        )
        .findFirst();

      Assert.assertTrue("LibormPerson mit ID= 2 gefunden: ", optionalperson.isPresent());

      Assert.assertEquals("Vorname von Satz-ID 2: ", "Testuser1", optionalperson.get().getVorname());

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testSelectList() {
    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {

      LibormPersonMapper mapper = new LibormPersonMapper();
      mapper.setConnection(connection);

      List<Criteria> criterias = new CriteriaBuilder().col(LibormPerson.col_ID).equals(1).build();

      List<LibormPerson> list = mapper.selectByCriterias(LibormPerson.class, LibormPerson.NAME, criterias);
      Assert.assertEquals("Anzahl der gelesenen Datens�tze: ", 1, list.size(), 0);

      Assert.assertEquals("Nachname des Satzen mit ID = 1: ", "Lenz", list.get(0).getNachname());

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testSubselect() {
    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {

      LibormPlzMapper plzmapper = new LibormPlzMapper();
      plzmapper.setConnection(connection);

      LibormPersonMapper personmapper = new LibormPersonMapper();
      personmapper.setConnection(connection);


      List<Criteria> criterias = new CriteriaBuilder().col(LibormPlz.col_ORT).equals("Usingen").build();

      plzmapper.selectByCriteriasStream(LibormPlz.class, LibormPlz.NAME, criterias).forEach(plz-> {
        try {
          List<Criteria> crits = new CriteriaBuilder().col(LibormPerson.col_PLZ_ID).equals(plz.getId()).build();
          long count = personmapper.selectByCriteriasStream(LibormPerson.class, LibormPerson.NAME, crits).count();
          Assert.assertEquals("Anzahl der Stadt Usingen in Tabelle Person", 2, count);
        } catch (SQLException e) {
          e.printStackTrace();
        }
      });

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
