package ormtest.model.generated.map;

import de.sebex.orm.BaseOrmMapper;
import de.sebex.orm.BaseOrmObject;
import ormtest.model.generated.dao.LibormPerson;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.sql.Clob;
import java.sql.Blob;
import java.util.List;

/**
 * Created by ormgenerator on 2017-10-31 12:25:59.018
 */
public class LibormPersonMapper extends BaseOrmMapper {

	public List<LibormPerson> selectAll() throws SQLException {
		String query = "select * from "+LibormPerson.NAME;
		return getListFromQuery(LibormPerson.class, query, isReadLobsOnSelect());
	}

	public Optional<LibormPerson> selectByPrimaryKey(Object id) throws SQLException {
		HashMap<String, Object> crits = new HashMap<>();
		crits.put(LibormPerson.col_ID, id);
		return super.selectByCrits(LibormPerson.class, LibormPerson.NAME, crits).stream().findFirst();
	}

	public void deleteByPrimaryKey(Object id) throws SQLException {
		HashMap<String, Object> crits = new HashMap<>();
		crits.put(LibormPerson.col_ID, id);
		super.deleteByCrits(LibormPerson.class, LibormPerson.NAME, crits);
	}

	public void insert(List<LibormPerson> list) throws SQLException {
		super.insert(LibormPerson.NAME, LibormPerson.COLS, list);
	}

	public void insert(LibormPerson item) throws SQLException {
		List<LibormPerson> list = new ArrayList<>();
		list.add(item);
		super.insert(LibormPerson.NAME, LibormPerson.COLS, list);
	}

	public void update(List<LibormPerson> list) throws SQLException {
		super.update(LibormPerson.NAME, LibormPerson.COLS, LibormPerson.KEYCOLS, list);
	}

	public void update(LibormPerson item) throws SQLException {
		List<LibormPerson> list = new ArrayList<>();
		list.add(item);
		super.update(LibormPerson.NAME, LibormPerson.COLS, LibormPerson.KEYCOLS, list);
	}


}
