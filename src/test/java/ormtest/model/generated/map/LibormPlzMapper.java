package ormtest.model.generated.map;

import de.sebex.orm.BaseOrmMapper;
import de.sebex.orm.BaseOrmObject;
import ormtest.model.generated.dao.LibormPlz;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.sql.Clob;
import java.sql.Blob;
import java.util.List;

/**
 * Created by ormgenerator on 2017-10-31 12:25:59.018
 */
public class LibormPlzMapper extends BaseOrmMapper {

	public List<LibormPlz> selectAll() throws SQLException {
		String query = "select * from "+LibormPlz.NAME;
		return getListFromQuery(LibormPlz.class, query, isReadLobsOnSelect());
	}

	public Optional<LibormPlz> selectByPrimaryKey(Object id) throws SQLException {
		HashMap<String, Object> crits = new HashMap<>();
		crits.put(LibormPlz.col_ID, id);
		return super.selectByCrits(LibormPlz.class, LibormPlz.NAME, crits).stream().findFirst();
	}

	public void deleteByPrimaryKey(Object id) throws SQLException {
		HashMap<String, Object> crits = new HashMap<>();
		crits.put(LibormPlz.col_ID, id);
		super.deleteByCrits(LibormPlz.class, LibormPlz.NAME, crits);
	}

	public void insert(List<LibormPlz> list) throws SQLException {
		super.insert(LibormPlz.NAME, LibormPlz.COLS, list);
	}

	public void insert(LibormPlz item) throws SQLException {
		List<LibormPlz> list = new ArrayList<>();
		list.add(item);
		super.insert(LibormPlz.NAME, LibormPlz.COLS, list);
	}

	public void update(List<LibormPlz> list) throws SQLException {
		super.update(LibormPlz.NAME, LibormPlz.COLS, LibormPlz.KEYCOLS, list);
	}

	public void update(LibormPlz item) throws SQLException {
		List<LibormPlz> list = new ArrayList<>();
		list.add(item);
		super.update(LibormPlz.NAME, LibormPlz.COLS, LibormPlz.KEYCOLS, list);
	}


}
