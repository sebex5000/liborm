package ormtest.model.generated.dao;

import de.sebex.orm.BaseOrmObject;
import de.sebex.orm.Cell;
import java.util.HashMap;
import java.util.Optional;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.sql.Clob;
import java.sql.Blob;

/**
 * Created by ormgenerator on 2017-10-31 12:25:59.018
 */
public class LibormPlz extends BaseOrmObject {

	public LibormPlz() {}
	public LibormPlz(HashMap<String, Cell> map) {
		setCells(map);
	}

	public static final String NAME="LIBORM_PLZ";

	public static final String col_ID = "id";
	public static final String col_ORT = "ort";
	public static final String col_BESCHREIBUNG = "beschreibung";

	public static final String[] COLS = new String[] { col_ID, col_ORT, col_BESCHREIBUNG };
	public static final Integer[] COLTYPES = new Integer[] { 4, 12, -1 };

	public static final String[] KEYCOLS = new String[] { col_ID };

	@Override
	public String[] getCols() {
		return COLS;
	}

	@Override
	public Integer[] getColTypes() {
		return COLTYPES;
	}

	public Integer getId() {
		return valueOfInteger(col_ID);
	}

	public String getOrt() {
		return valueOfString(col_ORT);
	}

	public String getBeschreibung() {
		return valueOfClob(col_BESCHREIBUNG);
	}


	public void setId(Integer value) {
		valueOfInteger(col_ID, value);
	}

	public void setOrt(String value) {
		valueOfString(col_ORT, value);
	}

	public void setBeschreibung(String value) {
		valueOfClob(col_BESCHREIBUNG, value);
	}

}
