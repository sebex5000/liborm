package ormtest.model.generated.dao;

import de.sebex.orm.BaseOrmObject;
import de.sebex.orm.Cell;
import java.util.HashMap;
import java.util.Optional;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.sql.Clob;
import java.sql.Blob;

/**
 * Created by ormgenerator on 2017-10-31 12:25:59.018
 */
public class LibormPerson extends BaseOrmObject {

	public LibormPerson() {}
	public LibormPerson(HashMap<String, Cell> map) {
		setCells(map);
	}

	public static final String NAME="LIBORM_PERSON";

	public static final String col_ID = "id";
	public static final String col_VORNAME = "vorname";
	public static final String col_NACHNAME = "nachname";
	public static final String col_GESCHLECHT = "geschlecht";
	public static final String col_GEB_DATUM = "geb_datum";
	public static final String col_PLZ_ID = "plz_id";

	public static final String[] COLS = new String[] { col_ID, col_VORNAME, col_NACHNAME, col_GESCHLECHT, col_GEB_DATUM, col_PLZ_ID };
	public static final Integer[] COLTYPES = new Integer[] { 4, 12, 12, 12, 91, 4 };

	public static final String[] KEYCOLS = new String[] { col_ID };

	@Override
	public String[] getCols() {
		return COLS;
	}

	@Override
	public Integer[] getColTypes() {
		return COLTYPES;
	}

	public Integer getId() {
		return valueOfInteger(col_ID);
	}

	public String getVorname() {
		return valueOfString(col_VORNAME);
	}

	public String getNachname() {
		return valueOfString(col_NACHNAME);
	}

	public String getGeschlecht() {
		return valueOfString(col_GESCHLECHT);
	}

	public Date getGebDatum() {
		return valueOfDate(col_GEB_DATUM);
	}

	public Integer getPlzId() {
		return valueOfInteger(col_PLZ_ID);
	}


	public void setId(Integer value) {
		valueOfInteger(col_ID, value);
	}

	public void setVorname(String value) {
		valueOfString(col_VORNAME, value);
	}

	public void setNachname(String value) {
		valueOfString(col_NACHNAME, value);
	}

	public void setGeschlecht(String value) {
		valueOfString(col_GESCHLECHT, value);
	}

	public void setGebDatum(Date value) {
		valueOfDate(col_GEB_DATUM, value);
	}

	public void setPlzId(Integer value) {
		valueOfInteger(col_PLZ_ID, value);
	}

}
