package ormtest;

import de.sebex.orm.dbconnection.DBConnectionFactory;
import de.sebex.orm.dbconnection.Lock;
import org.junit.Assert;
import org.junit.Test;
import ormtest.helper.TestConfig;
import ormtest.model.generated.dao.LibormPerson;
import ormtest.model.generated.dao.LibormPlz;
import ormtest.model.generated.map.LibormPersonMapper;
import ormtest.model.generated.map.LibormPlzMapper;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;


/**
 * Created by al13555 on 20.04.2017.
 */
public class InsertTest {

  @Test
  public void testInsert() throws SQLException {

    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {
      connection.setAutoCommit(false);

      LibormPersonMapper personmapper = new LibormPersonMapper();
      personmapper.setConnection(connection);


      try (Lock lock = dbConnectionFactory.getTablesLock(connection, LibormPerson.NAME)) {
        LibormPerson person = new LibormPerson();
        person.setId(10);
        person.setVorname("Vorname1");
        person.setNachname("Nachname1");
        person.setGebDatum(Date.valueOf(LocalDate.of(1980, 3, 21)));
        person.setPlzId(60385);

        personmapper.insert(person);

        Optional<LibormPerson> optional = personmapper.selectByPrimaryKey(10);
        Assert.assertTrue("Geschriebene Person kann nicht gelesen werden: ", optional.isPresent());
        if (optional.isPresent()) {
          LibormPerson person1 = optional.get();
          Assert.assertEquals("Vorname der geschriebenen Person: ", "Vorname1", person1.getVorname());
        }
      }

      connection.rollback();
    }
    catch (Exception ex) {
      throw ex;

    }

  }

  @Test
  public void testBlob () throws SQLException {

    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {
      connection.setAutoCommit(false);

      LibormPlzMapper mapper = new LibormPlzMapper();
      mapper.setConnection(connection);

      Optional<LibormPlz> optional = mapper.selectByPrimaryKey(61250);
      Assert.assertTrue("Person mit ID 1 wurde nicht gefunden.", optional.isPresent());

      if (optional.isPresent()) {
        LibormPlz ort = optional.get();
        Assert.assertTrue("Beschreibung ist leer: ", ort.getBeschreibung()==null);

        ort.setBeschreibung("Das ist eine schöne kleine Stadt im Hintertaunus.");
        mapper.update(ort);

      }
      connection.commit();
    }

    try (Connection connection = dbConnectionFactory.getConnection()) {
      connection.setAutoCommit(false);

      LibormPlzMapper mapper = new LibormPlzMapper();
      mapper.setConnection(connection);

      Optional<LibormPlz> optional1 = mapper.selectByPrimaryKey(61250);
      if (optional1.isPresent()) {
        LibormPlz plz = optional1.get();
        Assert.assertEquals("Ort mit PLZ 61250: ", "Usingen", plz.getOrt());
        Assert.assertTrue("Beschreibung startsWith: Das ist eine", plz.getBeschreibung().startsWith("Das ist eine"));
      }

      optional1.get().setBeschreibung(null);
      mapper.update(optional1.get());
      connection.commit();

    }


  }

}
