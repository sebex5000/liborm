package ormtest;


import de.sebex.orm.dbconnection.DBConnectionFactory;
import org.junit.Assert;
import org.junit.Test;
import ormtest.helper.TestConfig;
import ormtest.model.generated.dao.LibormPerson;
import ormtest.model.generated.dao.LibormPlz;
import ormtest.model.generated.map.LibormPersonMapper;
import ormtest.model.generated.map.LibormPlzMapper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Created by al13555 on 20.04.2017.
 */
public class UpdateTest {

  @Test
  public void testUpdate() throws SQLException{

    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {

      connection.setAutoCommit(false);

      LibormPersonMapper personMapper = new LibormPersonMapper();
      personMapper.setConnection(connection);

      Optional<LibormPerson> optional = personMapper.selectByPrimaryKey(1);
      Assert.assertTrue("Person mit ID 1 wurde nicht gefunden.", optional.isPresent());

      if (optional.isPresent()) {
        LibormPerson person = optional.get();

        Assert.assertEquals("Nachname der Person mit ID=1: ", "Lenz", person.getNachname());

        person.setNachname("K�stner");

        personMapper.update(person);

        Optional<LibormPerson> optional1 = personMapper.selectByPrimaryKey(1);
        if (optional1.isPresent()) {
          LibormPerson person1 = optional1.get();

          Assert.assertEquals("Person nach update: ", "K�stner", person1.getNachname());
        }

      }
      connection.rollback();

    }
  }


  @Test
  public void testClob () throws SQLException {
    DBConnectionFactory dbConnectionFactory = TestConfig.getConnectionFactory();
    try (Connection connection = dbConnectionFactory.getConnection()) {
      connection.setAutoCommit(false);

      LibormPlzMapper mapper = new LibormPlzMapper();
      mapper.setConnection(connection);

      Optional<LibormPlz> optional = mapper.selectByPrimaryKey(61250);
      Assert.assertTrue("Person mit ID 1 wurde nicht gefunden.", optional.isPresent());

      if (optional.isPresent()) {
        LibormPlz ort = optional.get();
        Assert.assertFalse("Beschreibung ist gefüllt: ", ort.getBeschreibung()!=null);

        ort.setBeschreibung("Das ist eine schöne kleine Stadt im Hintertaunus.");
        mapper.update(ort);
        connection.commit();
      }
    }

    try (Connection connection = dbConnectionFactory.getConnection()) {
      connection.setAutoCommit(false);

      LibormPlzMapper mapper = new LibormPlzMapper();
      mapper.setConnection(connection);

      Optional<LibormPlz> optional1 = mapper.selectByPrimaryKey(61250);
      if (optional1.isPresent()) {
        LibormPlz plz = optional1.get();
        Assert.assertEquals("Ort mit PLZ 61250: ", "Usingen", plz.getOrt());
        Assert.assertTrue("Beschreibung startsWith: Das ist eine", optional1.get().getBeschreibung().startsWith("Das ist eine"));
      }

      optional1.get().setBeschreibung(null);
      mapper.update(optional1.get());
      connection.commit();

    }


  }




}
