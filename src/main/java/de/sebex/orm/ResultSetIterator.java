package de.sebex.orm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

public class ResultSetIterator implements Iterator {

  private ResultSet resultSet;
  private Statement statement;
  private Class<? extends BaseOrmObject> returnObjectClass = null;
  private BaseOrmMapper mapper=null;

  public <T extends BaseOrmObject> ResultSetIterator(Class<T> returnObjectClass, BaseOrmMapper mapper, ResultSet resultSet, Statement statement) {
    this.resultSet = resultSet;
    this.statement = statement;
    this.returnObjectClass = returnObjectClass;
    this.mapper = mapper;
  }

  public void init() {
  }

  @Override
  public boolean hasNext() {
    try {
      boolean hasMore = this.resultSet.next();
      if (!hasMore) {
        close();
      }
      return hasMore;
    } catch (SQLException e) {
      close();
      throw new RuntimeException("Fehler bei hasNext() "+e.getMessage(), e);
    }

  }

  private void close() {
    try {
      this.resultSet.close();
      try {
        this.statement.close();
      } catch (SQLException e) {
        //nothing we can do here
      }
    } catch (SQLException e) {
      //nothing we can do here
    }
  }

  @Override
  public BaseOrmObject next() {
    try {
      BaseOrmObject o = this.returnObjectClass.newInstance();
      o.setCells(mapper.createRowFromResultSet(this.resultSet, mapper.isReadLobsOnSelect()));
      return o;
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    } catch (InstantiationException e) {
      throw new RuntimeException(e);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}