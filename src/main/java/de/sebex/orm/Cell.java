package de.sebex.orm;

/**
 * Created by al13555 on 22.05.2017.
 */
public class Cell {
  public String label = null;
  public String name = null;
  public int columnType = 0;
  public Object value = null;
  public boolean primaryKey = false;

  @Override
  public String toString() {
    return this.label;
  }

  public String getLabel() {
    String lbl = label.substring(0, 1).toUpperCase() + label.substring(1).toLowerCase();
    int lodashpos = -1;
    while ((lodashpos = lbl.indexOf("_")) > 0) {
      lbl = lbl.substring(0, lodashpos) + lbl.substring(lodashpos + 1, lodashpos + 2).toUpperCase() + lbl.substring(lodashpos + 2).toLowerCase();
    }
    return lbl;
  }
}