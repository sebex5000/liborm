package de.sebex.orm.gradleplugins;

import de.sebex.orm.Cell;
import de.sebex.orm.TableInfo;
import de.sebex.orm.dbconnection.*;
import de.sebex.orm.dbconnection.mysql.MySqlConnectionFactory;
import de.sebex.orm.dbconnection.oracle.OracleConnectionFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.Date;

  /**
   * Created by al13555 on 13.03.2017.
   */
  public class OrmClassGenerator {

    private static Map<Integer, Class> typemap = null;
    private static Map<Integer, String> functionmap = null;

    static {
      typemap = new HashMap<>();
      typemap.put(Types.TINYINT, Byte.class);
      typemap.put(Types.SMALLINT, Short.class);
      typemap.put(Types.INTEGER, Integer.class);
      typemap.put(Types.BIGINT, Long.class);
      typemap.put(Types.FLOAT, Double.class);
      typemap.put(Types.DOUBLE, Double.class);
      typemap.put(Types.DECIMAL, BigDecimal.class);
      typemap.put(Types.NUMERIC, BigDecimal.class);
      typemap.put(Types.VARCHAR, String.class);
      typemap.put(Types.CHAR, String.class);
      typemap.put(Types.BIT, Boolean.class);
      typemap.put(Types.BOOLEAN, Boolean.class);
      typemap.put(Types.DATE, Date.class);
      typemap.put(Types.TIMESTAMP, Timestamp.class);
      typemap.put(Types.LONGVARCHAR, String.class);
      typemap.put(Types.CLOB, String.class);
      typemap.put(Types.BLOB, byte[].class);

      functionmap = new HashMap<>();
      functionmap.put(Types.TINYINT, "valueOfByte");
      functionmap.put(Types.SMALLINT, "valueOfShort");
      functionmap.put(Types.INTEGER, "valueOfInteger");
      functionmap.put(Types.BIGINT, "valueOfLong");
      functionmap.put(Types.FLOAT, "valueOfDouble");
      functionmap.put(Types.DOUBLE, "valueOfDouble");
      functionmap.put(Types.DECIMAL, "valueOfBigDecimal");
      functionmap.put(Types.NUMERIC, "valueOfBigDecimal");
      functionmap.put(Types.VARCHAR, "valueOfString");
      functionmap.put(Types.CHAR, "valueOfString");
      functionmap.put(Types.BIT, "valueOfBoolean");
      functionmap.put(Types.BOOLEAN, "valueOfBoolean");
      functionmap.put(Types.DATE, "valueOfDate");
      functionmap.put(Types.TIMESTAMP, "valueOfTimestamp");
      functionmap.put(Types.LONGVARCHAR, "valueOfClob");
      functionmap.put(Types.CLOB, "valueOfClob");
      functionmap.put(Types.BLOB, "valueOfBlob");


    }

    private String getMappedType(int jdbctype) {
      if (typemap.containsKey(jdbctype)) {
        return typemap.get(jdbctype).getSimpleName();
      } else return String.class.getSimpleName();
    }

    private String getMappedFunction(int jdbctype) {
      if (functionmap.containsKey(jdbctype)) {
        return functionmap.get(jdbctype);
      }
      throw new IllegalArgumentException("Keine Methode für den JdbcType " + jdbctype + " gefunden.");
    }


    private String getTableName(String object_name) {
      String tableName = object_name.substring(0, 1).toUpperCase() + object_name.substring(1).toLowerCase();
      int lodashpos = -1;
      while ((lodashpos = tableName.indexOf("_")) > 0) {
        tableName = tableName.substring(0, lodashpos) + tableName.substring(lodashpos + 1, lodashpos + 2).toUpperCase() + tableName.substring(lodashpos + 2).toLowerCase();
      }
      return tableName;
    }

    private static String now = null;

    static {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
      now = simpleDateFormat.format(Date.from(Instant.now()));
    }

    private File outputDir = null;
    private String dbURL = "";
    private String dbUser = null;
    private String dbSchema = null;
    private String dbPassword = null;
    private String targetNamespace = null;
    private String tablenamepattern = null;
    private String dbtype = null;
    private List<String> tablenames = null;

    public List<String> gettablenames() {
      return tablenames;
    }

    public OrmClassGenerator settablenames(List<String> tablenames) {
      this.tablenames = tablenames;
      return this;
    }

    public String gettablenamepattern() {
      return tablenamepattern;
    }

    public OrmClassGenerator settablenamepattern(String tablenamepattern) {
      this.tablenamepattern = tablenamepattern;
      return this;
    }


    public OrmClassGenerator setoutputdir(File outputDir) {
      this.outputDir = outputDir;
      return this;
    }


    public OrmClassGenerator setdburl(String value) {
      this.dbURL = value;
      return this;
    }


    public File getOutputDir() {
      return this.outputDir;
    }


    public String getdbuser() {
      return dbUser;
    }

    public OrmClassGenerator setdbuser(String dbUser) {
      this.dbUser = dbUser;
      return this;
    }

    public String getDbType() {
      return this.dbtype;
    }

    public OrmClassGenerator setDbType(String dbtype) {
      this.dbtype = dbtype;
      return this;
    }

    public String getdbpassword() {
      return dbPassword;
    }

    public OrmClassGenerator setdbpassword(String dbPassword) {
      this.dbPassword = dbPassword;
      return this;
    }

    public String getdbschema() {
      return dbSchema;
    }

    public OrmClassGenerator setdbschema(String dbSchema) {
      this.dbSchema = dbSchema;
      return this;
    }

    private File getResultOutputDir(String appendix) {
      File resultdir = new File(getOutputDir(), (gettargetnamespace(appendix)).replace('.', '/'));
      if (resultdir.exists() == false) resultdir.mkdirs();
      return resultdir;
    }

    public String gettargetnamespace(String appendix) {

      if (appendix != null && appendix.isEmpty() == false)
        return targetNamespace + "." + appendix;
      else
        return targetNamespace;
    }

    public de.sebex.orm.gradleplugins.OrmClassGenerator settargetnamespace(String targetNamespace) {
      this.targetNamespace = targetNamespace;
      return this;
    }


    public void process() throws Exception {

      DSConnectionParams cp = new DSConnectionParams(dbURL, getdbuser(), getdbpassword(), getdbschema());
      DBConnectionFactory connFactory = null;

      DBType dbt = DBType.valueOf(getDbType());
      switch (dbt) {
        case MYSQL:
          connFactory = new MySqlConnectionFactory(cp);
          break;
        case ORACLE:
          connFactory = new OracleConnectionFactory(cp);
          break;
        default:
          break;
      }

      try (Connection conn = connFactory.getConnection()) {
        List<TableInfo> tableinfos = connFactory.getTableInfos(cp.getSchema(), gettablenamepattern());
        tableinfos.forEach(tableinfo -> {
          System.out.println(">>> Processing table " + tableinfo.tableName + " ...");

          processDao(conn, tableinfo);
          processMap(conn, tableinfo);
        });
      }
    }


    public void processMap(Connection connection, TableInfo tableInfo) {

      String ln = System.lineSeparator();
      String tableName = getTableName(tableInfo.tableName);

      List<Cell> cells = tableInfo.getCells();

      StringBuilder t = new StringBuilder();
      t.append("package " + gettargetnamespace("map") + ";");
      t.append(ln);
      t.append(ln);
      t.append("import de.sebex.orm.BaseOrmMapper;");
      t.append(ln);
      t.append("import de.sebex.orm.BaseOrmObject;");
      t.append(ln);
      t.append("import " + gettargetnamespace("dao") + "." + tableName + ";");
      t.append(ln);
      t.append("import java.sql.*;");
      t.append(ln);
      t.append("import java.util.ArrayList;");
      t.append(ln);
      t.append("import java.util.HashMap;");
      t.append(ln);
      t.append("import java.util.Optional;");
      t.append(ln);
      t.append("import java.math.BigDecimal;");
      t.append(ln);
      t.append("import java.sql.Date;");
      t.append(ln);
      t.append("import java.sql.Timestamp;");
      t.append(ln);
      t.append("import java.sql.Clob;");
      t.append(ln);
      t.append("import java.sql.Blob;");
      t.append(ln);
      t.append("import java.util.List;");
      t.append(ln);
      t.append(ln);
      t.append("/**\n" +
              " * Created by ormgenerator on " + now + "\n" +
              " */");
      t.append(ln);
      t.append("public class " + tableName + "Mapper extends BaseOrmMapper {");
      t.append(ln);
      t.append(ln);

      t.append("\tpublic List<" + tableName + "> selectAll() throws SQLException {");
      t.append(ln);
      t.append("\t\tString query = \"select * from \"+"+tableName+".NAME;");
      t.append(ln);
      t.append("\t\treturn getListFromQuery(" + tableName + ".class, query, isReadLobsOnSelect());");
      t.append(ln);
      t.append("\t}");
      t.append(ln);
      t.append(ln);

      //-------------------------------------------------------------------------------
      String whereString = null;

      List<String> keycollist = new ArrayList<>();
      List<String> keycollistpure = new ArrayList<>();
      cells.stream().filter(i-> i.primaryKey).forEach(c -> {
        keycollist.add("Object " + c.name);
        keycollistpure.add(c.name);
      });
      String paramString = String.join(", ", keycollist);

      t.append("\tpublic Optional<" + tableName + "> selectByPrimaryKey("+paramString+") throws SQLException {");
      t.append(ln);
      t.append("\t\tHashMap<String, Object> crits = new HashMap<>();");
      t.append(ln);
      for(String keycol : keycollistpure) {
        t.append("\t\tcrits.put("+tableName+".col_"+keycol.toUpperCase()+", "+keycol+");");
        t.append(ln);
      }
      t.append("\t\treturn super.selectByCrits("+tableName+".class, "+tableName+".NAME, crits).stream().findFirst();");
      t.append(ln);
      t.append("\t}");
      t.append(ln);
      t.append(ln);

      //************ delete
      t.append("\tpublic void deleteByPrimaryKey("+paramString+") throws SQLException {");
      t.append(ln);
      t.append("\t\tHashMap<String, Object> crits = new HashMap<>();");
      t.append(ln);
      for(String keycol : keycollistpure) {
        t.append("\t\tcrits.put("+tableName+".col_"+keycol.toUpperCase()+", "+keycol+");");
        t.append(ln);
      }
      t.append("\t\tsuper.deleteByCrits("+tableName+".class, "+tableName+".NAME, crits);");
      t.append(ln);
      t.append("\t}");
      t.append(ln);
      t.append(ln);


      //-------------------------------------------------------------------------------

      t.append("\tpublic void insert(List<" + tableName + "> list) throws SQLException {");
      t.append(ln);
      t.append("\t\tsuper.insert("+tableName+".NAME, "+tableName+".COLS, list);");
      t.append(ln);
      t.append("\t}");
      t.append(ln);
      t.append(ln);

      //-------------------------------------------------------------------------------

      t.append("\tpublic void insert(" + tableName + " item) throws SQLException {");
      t.append(ln);
      t.append("\t\tList<"+tableName+"> list = new ArrayList<>();");
      t.append(ln);
      t.append("\t\tlist.add(item);");
      t.append(ln);
      t.append("\t\tsuper.insert("+tableName+".NAME, "+tableName+".COLS, list);");
      t.append(ln);
      t.append("\t}");
      t.append(ln);
      t.append(ln);

      //-------------------------------------------------------------------------------

      t.append("\tpublic void update(List<" + tableName + "> list) throws SQLException {");
      t.append(ln);
      t.append("\t\tsuper.update("+tableName+".NAME, "+tableName+".COLS, "+tableName+".KEYCOLS, list);");
      t.append(ln);
      t.append("\t}");
      t.append(ln);
      t.append(ln);

      //-------------------------------------------------------------------------------

      t.append("\tpublic void update(" + tableName + " item) throws SQLException {");
      t.append(ln);
      t.append("\t\tList<"+tableName+"> list = new ArrayList<>();");
      t.append(ln);
      t.append("\t\tlist.add(item);");
      t.append(ln);
      t.append("\t\tsuper.update("+tableName+".NAME, "+tableName+".COLS, "+tableName+".KEYCOLS, list);");
      t.append(ln);
      t.append("\t}");
      t.append(ln);
      t.append(ln);

      //-------------------------------------------------------------------------------

      t.append(ln);

      t.append("}");
      t.append(ln);

      File file = new File(getResultOutputDir("map"), tableName + "Mapper.java");
      if (file.exists()) {
        System.out.println("\tERROR map/" + file.getName() + " exists.");
      } else {
        try (FileWriter writer = new FileWriter(file)) {
          writer.write(t.toString());
          System.out.println("\tOK map/" + file.getName() + " created.");
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    public void processDao(Connection connection, TableInfo tableinfo) {
      String ln = System.lineSeparator();

      String tableName = getTableName(tableinfo.tableName);

      List<Cell> cells = tableinfo.getCells();

      StringBuilder t = new StringBuilder();
      t.append("package " + gettargetnamespace("dao") + ";");
      t.append(ln);
      t.append(ln);
      t.append("import de.sebex.orm.BaseOrmObject;");
      t.append(ln);
      t.append("import de.sebex.orm.Cell;");
      t.append(ln);
      t.append("import java.util.HashMap;");
      t.append(ln);
      t.append("import java.util.Optional;");
      t.append(ln);
      t.append("import java.math.BigDecimal;");
      t.append(ln);
      t.append("import java.sql.Date;");
      t.append(ln);
      t.append("import java.sql.Timestamp;");
      t.append(ln);
      t.append("import java.sql.Clob;");
      t.append(ln);
      t.append("import java.sql.Blob;");
      t.append(ln);
      t.append(ln);
      t.append("/**\n" +
              " * Created by ormgenerator on " + now + "\n" +
              " */");
      t.append(ln);
      t.append("public class " + tableName + " extends BaseOrmObject {");
      t.append(ln);
      t.append(ln);
      t.append("\tpublic " + tableName + "() {}");
      t.append(ln);
      t.append("\tpublic " + tableName + "(HashMap<String, Cell> map) {\n" +
              "\t\tsetCells(map);\n" +
              "\t}");
      t.append(ln);
      t.append(ln);
      t.append(String.format("\tpublic static final String NAME=\"%s\";", tableinfo.tableName));
      t.append(ln);
      t.append(ln);

      for (Cell cell : cells) {
        t.append(String.format("\tpublic static final String col_%s = \"%s\";", cell.label.toUpperCase(), cell.label));
        t.append(ln);
      }
      t.append(ln);


      List<String> collist = new ArrayList<>();
      List<String> colTypList = new ArrayList<>();
      cells.forEach(c -> {
        collist.add("col_" + c.name.toUpperCase());
        colTypList.add(String.valueOf(c.columnType));
      } );
      t.append(String.format("\tpublic static final String[] COLS = new String[] { %s };", String.join(", ", collist)));
      t.append(ln);
      t.append(String.format("\tpublic static final Integer[] COLTYPES = new Integer[] { %s };", String.join(", ", colTypList)));
      t.append(ln);
      t.append(ln);

      List<String> keycollist = new ArrayList<>();
      cells.stream().filter(i-> i.primaryKey).forEach(c -> keycollist.add("col_" + c.name.toUpperCase()));
      t.append(String.format("\tpublic static final String[] KEYCOLS = new String[] { %s };", String.join(", ", keycollist)));
      t.append(ln);
      t.append(ln);

      t.append(String.format("\t@Override%n"));
      t.append(String.format("\tpublic String[] getCols() {%n"));
      t.append(String.format("\t\treturn COLS;%n"));
      t.append(String.format("\t}%n"));
      t.append(ln);

      t.append(String.format("\t@Override%n"));
      t.append(String.format("\tpublic Integer[] getColTypes() {%n"));
      t.append(String.format("\t\treturn COLTYPES;%n"));
      t.append(String.format("\t}%n"));
      t.append(ln);

      for (Cell cell : cells) {

        t.append("\tpublic " + getMappedType(cell.columnType) + " get" + cell.getLabel() + "() {");
        t.append(ln);
        t.append("\t\treturn " + getMappedFunction(cell.columnType) + "(col_" + cell.name.toUpperCase() + ");");
        t.append(ln);
        t.append("\t}");
        t.append(ln);
        t.append(ln);
      }
      t.append(ln);

      for (Cell cell : cells) {

        t.append("\tpublic void set" + cell.getLabel() + "(" + getMappedType(cell.columnType) + " value) {");
        t.append(ln);
        t.append("\t\t" + getMappedFunction(cell.columnType) + "(col_" + cell.name.toUpperCase() + ", value);");
        t.append(ln);
        t.append("\t}");
        t.append(ln);
        t.append(ln);
      }

      t.append("}");
      t.append(ln);

      File file = new File(getResultOutputDir("dao"), tableName + ".java");
      if (file.exists()) {
        System.out.println("\tERROR dao/" + file.getName() + " exists.");
      } else {
        try (FileWriter writer = new FileWriter(file)) {
          writer.write(t.toString());
          System.out.println("\tOK dao/" + file.getName() + " created.");
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }



    public static void main(String[] args) {
      try {
        new OrmClassGenerator()
                .setdburl(args[0])
                .setdbuser(args[1])
                .setdbpassword(args[2])
                .setdbschema(args[3])
                .settablenamepattern(args[4])
                .setoutputdir(new File(args[5]))
                .settargetnamespace(args[6])
                .setDbType(args[7].toUpperCase())
                .process();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }
