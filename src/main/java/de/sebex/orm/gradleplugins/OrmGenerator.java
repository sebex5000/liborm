package de.sebex.orm.gradleplugins;


import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.util.List;

/**
 * Created by al13555 on 13.03.2017.
 */
public class OrmGenerator extends DefaultTask {

  private File outputDir = null;

  private String dbURL = "";
  private String dbUser = null;
  private String dbSchema = null;
  private String dbPassword = null;
  private String dbType= null;
  private String targetNamespace = null;
  private String tablenamepattern = null;
  private List<String> tablenames = null;

  public List<String> gettablenames() {
    return tablenames;
  }

  public OrmGenerator settablenames(List<String> tablenames) {
    this.tablenames = tablenames;
    return this;
  }

  @Input
  public String getdbschema() {
    return this.dbSchema;
  }

  @Input
  public String getDbType() {
    return this.dbType;
  }

  public OrmGenerator setDbType(String dbType) {
    this.dbType = dbType;
    return this;
  }

  public OrmGenerator setDbSchema(String value) {
    this.dbSchema = value;
    return this;
  }

  @Input
  public String gettablenamepattern() {
    return tablenamepattern;
  }


  public OrmGenerator setTableNamepattern(String tablenamepattern) {
    this.tablenamepattern = tablenamepattern;
    return this;
  }


  public OrmGenerator setOutputDir(File outputDir) {
    this.outputDir = outputDir;
    return this;
  }

  @Input
  public String getDbURL() {
    return dbURL;
  }


  public OrmGenerator setDbURL(String dbURL) {
    this.dbURL = dbURL;
    return this;
  }

  @OutputDirectory
  public File getOutputDir() {
    return this.outputDir;
  }

  @Input
  public String getDbUser() {
    return dbUser;
  }

  public OrmGenerator setDbUser(String dbUser) {
    this.dbUser = dbUser;
    return this;
  }

  @Input
  public String getDbPassword() {
    return dbPassword;
  }

  public OrmGenerator setDbPassword(String dbPassword) {
    this.dbPassword = dbPassword;
    return this;
  }

  @TaskAction
  public void process() throws Exception {

    new OrmClassGenerator()
      .setdburl(getDbURL())
      .setdbuser(getDbUser())
      .setdbschema(getdbschema())
      .setdbpassword(getDbPassword())
      .settargetnamespace(getTargetNamespace())
      .settablenamepattern(gettablenamepattern())
      .setoutputdir(getOutputDir())
      .setDbType(getDbType())
      .process();
  }

  @Input
  public String getTargetNamespace() {
    return targetNamespace;
  }

  public OrmGenerator setTargetNamespace(String targetNamespace) {
    this.targetNamespace = targetNamespace;
    return this;
  }


}
