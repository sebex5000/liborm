package de.sebex.orm;

import java.util.List;

/**
 * Created by al13555 on 22.05.2017.
 */
public class TableInfo {

  public String tableName = null;
  public String schema = null;
  private List<Cell> cells = null;

  public List<Cell> getCells() {
    return cells;
  }

  public void setCells(List<Cell> cells) {
    this.cells = cells;
  }
}
