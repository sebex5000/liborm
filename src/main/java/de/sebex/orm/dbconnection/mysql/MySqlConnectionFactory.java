package de.sebex.orm.dbconnection.mysql;


import de.sebex.orm.Cell;
import de.sebex.orm.TableInfo;
import de.sebex.orm.dbconnection.DBConnectionFactory;
import de.sebex.orm.dbconnection.DSConnectionParams;
import de.sebex.orm.dbconnection.Lock;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MySqlConnectionFactory extends DBConnectionFactory {

  private DSConnectionParams cp = null;

  private static final String MySqlDriver = "com.mysql.jdbc.Driver";
  private static final String ConnectionStringTemplate = "jdbc:mysql://%s/%s?%s";
  private String ConnectionString = null;

  public MySqlConnectionFactory(DSConnectionParams cp) {
    this.cp = cp;

    HashMap<String, String> properties = new HashMap<>();
    properties.put("useJDBCCompliantTimezoneShift", "true");
    properties.put("useLegacyDatetimeCode", "false");
    properties.put("serverTimezone", "UTC");

    StringBuilder sb = new StringBuilder();

    properties.forEach((key, value) -> {
      if (sb.toString().length() > 0) sb.append("&");
      sb.append(key);
      sb.append("=");
      sb.append(value);
    });

    this.ConnectionString = String.format(ConnectionStringTemplate, cp.dbUrl, cp.schema, sb.toString() );




  }

  public Connection getConnection() {
    try {
      Class.forName(MySqlDriver).newInstance();
      Connection connection = DriverManager.getConnection(this.ConnectionString, cp.getUsername(), cp.getPassword());
      connection.setAutoCommit(false);
      return connection;
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  @Override
  public List<TableInfo> getTableInfos(String schema, String tableNamePattern) {
    List<TableInfo> tableInfos = new ArrayList<>();
    try (Connection conn = getConnection()) {
      try (Statement stmt = conn.createStatement()) {
        String sql = String.format("select * from information_schema.TABLES where TABLE_SCHEMA = '%s' and table_name like '%s'", schema, tableNamePattern);
        try (ResultSet tableresult = stmt.executeQuery(sql)) {
          while (tableresult.next()) {
            TableInfo tableInfo = new TableInfo();
            tableInfo.tableName = tableresult.getString("table_name");
            tableInfo.schema = tableresult.getString("table_schema");
            tableInfo.setCells(getCells(conn, tableInfo.schema, tableInfo.tableName));
            tableInfos.add(tableInfo);
          }
        }
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }

    return tableInfos;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName()+":"+":"+cp.schema;
  }

  @Override
  public List<Cell> getCells(Connection connection, String owner, String object_name) {
    String sql = String.format("select * from %s.%s where 1=2", owner, object_name);
    List<Cell> cells = new ArrayList<>();
    List<String> keyCols = new ArrayList<>();

    try {
      DatabaseMetaData dbmeta = connection.getMetaData();
      try (ResultSet rs = dbmeta.getPrimaryKeys(null, null, object_name)) {
        while (rs.next()) {
          keyCols.add(rs.getString("COLUMN_NAME"));
        }
      }
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }

    try (Statement stmt = connection.createStatement()) {
      try (ResultSet rs = stmt.executeQuery(sql)) {

        ResultSetMetaData metaData = rs.getMetaData();
        if (metaData != null) {
          for (int i = 1; i <= metaData.getColumnCount(); i++) {
            Cell cell = new Cell();
            int columnType = metaData.getColumnType(i);
            String colname = metaData.getColumnName(i);
            String label = metaData.getColumnLabel(i);
            if (keyCols.contains(colname)) cell.primaryKey = true;
            cell.label = label;
            cell.name = colname;
            cell.columnType = columnType;
            cells.add(cell);
          }
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return cells;
  }

  @Override
  public Lock getTablesLock(Connection connection, String... tablenames) {
    return new MySqlLock(connection, tablenames);
  }


}
