package de.sebex.orm.dbconnection.mysql;

import de.sebex.orm.dbconnection.Lock;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlLock extends Lock {

  public MySqlLock(Connection connection, String[] tablenames) {
    super(connection, tablenames);
    lock();
  }

  public void lock() {
    try {
      try (Statement stmt = getConnection().createStatement()) {
        StringBuilder sb = new StringBuilder();
        for (String tablename : getTablenames()) {
          if (sb.length() > 0)  {
            sb.append(", ");
          }
          sb.append(tablename);
          sb.append(" WRITE");
        }

        stmt.execute("lock tables "+sb.toString());
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void unlock() {
    try {
      try (Statement stmt = getConnection().createStatement()) {
        stmt.execute("unlock tables");
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }


}
