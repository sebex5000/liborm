package de.sebex.orm.dbconnection;


import de.sebex.orm.Cell;
import de.sebex.orm.TableInfo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sebastian on 19.03.17.
 */
public abstract class DBConnectionFactory {

  public abstract Connection getConnection();
  public abstract List<TableInfo> getTableInfos(String schema, String tableNamePattern);
  public abstract List<Cell> getCells(Connection connection, String owner, String object_name);
  public abstract Lock getTablesLock(Connection connection, String...tablenames);

}
