package de.sebex.orm.dbconnection.oracle;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.mysql.jdbc.NotImplemented;
import de.sebex.orm.Cell;
import de.sebex.orm.TableInfo;
import de.sebex.orm.dbconnection.DBConnectionFactory;
import de.sebex.orm.dbconnection.DSConnectionParams;
import de.sebex.orm.dbconnection.Lock;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by al13555 on 07.12.2016.
 */
public class OracleConnectionFactory extends DBConnectionFactory {

//    static Logger logger = LoggerFactory.getLogger(DataSourceOracleFactory.class);


    private DSConnectionParams cp = null;

    private static final String OracleDriver = "oracle.jdbc.OracleDriver";
    private static String dbConnectionStringTemplate = "jdbc:oracle:thin:@%s";

    private String connectionString = null;
    static {
        String TNS_Names = System.getenv("TNS_ADMIN");
        System.setProperty("oracle.net.tns_admin", TNS_Names);
    }

    public OracleConnectionFactory(DSConnectionParams cp) {
        this.cp = cp;
        this.connectionString = String.format(dbConnectionStringTemplate, cp.getDbUrl());
    }

    public Connection getConnection() {
        try {
            Class clazz = Class.forName(OracleDriver);
            Connection connection = DriverManager.getConnection(this.connectionString, cp.getUsername(), cp.getPassword());
            return connection;
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<TableInfo> getTableInfos(String schema, String tableNamePattern) {
        List<TableInfo> tableInfos = new ArrayList<>();
        try (Connection conn = getConnection()) {
            try (Statement stmt = conn.createStatement()) {
                String sql = String.format("select * from sys.ALL_OBJECTS where owner = '%s' and object_type in ('TABLE') and object_name like '%s'", schema, tableNamePattern);
                try (ResultSet tableresult = stmt.executeQuery(sql)) {
                    while (tableresult.next()) {
                        TableInfo tableInfo = new TableInfo();
                        tableInfo.tableName = tableresult.getString("object_name");
                        tableInfo.schema = tableresult.getString("owner");
                        tableInfo.setCells(getCells(conn, tableInfo.schema, tableInfo.tableName));
                        tableInfos.add(tableInfo);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return tableInfos;
    }


    @Override
    public String toString() {
    	return this.getClass().getSimpleName()+":"+connectionString+":"+cp.schema;
    }

    @Override
    public List<Cell> getCells(Connection connection, String owner, String object_name) {
        String sql = String.format("select * from %s.%s where 1=2", owner, object_name);
        List<Cell> cells = new ArrayList<>();
        List<String> keyCols = new ArrayList<>();

        try {
            DatabaseMetaData dbmeta = connection.getMetaData();
            try (ResultSet rs = dbmeta.getPrimaryKeys(null, null, object_name)) {
                while (rs.next()) {
                    keyCols.add(rs.getString("COLUMN_NAME"));
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        try (Statement stmt = connection.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sql)) {

                ResultSetMetaData metaData = rs.getMetaData();
                if (metaData != null) {
                    for (int i = 1; i <= metaData.getColumnCount(); i++) {
                        Cell cell = new Cell();
                        int columnType = metaData.getColumnType(i);
                        String colname = metaData.getColumnName(i);
                        String label = metaData.getColumnLabel(i);
                        if (keyCols.contains(colname)) cell.primaryKey = true;
                        cell.label = label;
                        cell.name = colname;
                        cell.columnType = columnType;
                        cells.add(cell);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cells;
    }

    @Override
    public Lock getTablesLock(Connection connection, String... tablenames) {
        try {
            throw new NotImplemented();
        } catch (NotImplemented throwables) {
            throw new RuntimeException(throwables);
        }
    }
}
