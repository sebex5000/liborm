package de.sebex.orm.dbconnection.oracle;

import de.sebex.orm.dbconnection.Lock;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class OracleLock extends Lock {

  public OracleLock(Connection connection, String[] tablenames) {
    super(connection, tablenames);
    lock();
  }

  public void lock() {
    try {
      try (Statement stmt = getConnection().createStatement()) {
        for (String tablename: getTablenames()) {
          stmt.execute("lock table " + tablename + " in exclusive mode");
        }
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void unlock() {

  }


}
