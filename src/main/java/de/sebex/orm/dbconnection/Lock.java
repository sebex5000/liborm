package de.sebex.orm.dbconnection;

import java.sql.Connection;

public abstract class Lock implements AutoCloseable {

  public Connection getConnection() {
    return connection;
  }

  public String[] getTablenames() {
    return tablenames;
  }

  Connection connection = null;
  String[] tablenames = null;

  public Lock(Connection connection, String[] tablenames) {
    this.connection = connection;
    this.tablenames = tablenames;
  }

  public abstract void lock();
  public abstract void unlock();

  @Override
  public void close() {
    this.unlock();
  }



}
