package de.sebex.orm.dbconnection;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

public class DSConnectionParams implements Cloneable, Serializable {

  private static final long serialVersionUID = 1288815790814483223L;

  public String user = "";
  public String password = "";
  public String schema = "";
  public String jndiName = "";

  public String getDbUrl() {
    return dbUrl;
  }

  public void setDbUrl(String dbUrl) {
    this.dbUrl = dbUrl;
  }

  public String dbUrl = "";

  public String getJndiName() {
    return jndiName;
  }

  public void setJndiName(String jndiName) {
    this.jndiName = jndiName;
  }


  private Properties info = null;


  public DSConnectionParams() {
  }

  public DSConnectionParams(String dbUrl, String user, String password, String schema) {
    this.dbUrl = dbUrl;
    this.user = user;
    this.password = password;
    this.schema = schema;
  }

  public DSConnectionParams(DSConnectionParams cp) {
    user = cp.user;
    password = cp.password;
    schema = cp.schema;
    info = new Properties();

    for (Map.Entry<Object, Object> e : cp.info.entrySet()) {
      info.setProperty((String) e.getKey(), (String) e.getValue());
    }

  }

  public Object clone() throws CloneNotSupportedException {
    DSConnectionParams cloned = (DSConnectionParams) super.clone();
//     cloned.list   = list;
//     cloned.i_next = (Item) i_next.clone();

    for (Map.Entry<Object, Object> e : info.entrySet()) {
      cloned.info.setProperty((String) e.getKey(), (String) e.getValue());
    }

    return cloned;
  }

  public void setProperty(String name, String value) {
    info.setProperty(name, value);
  }

  public Properties getProperties() {

    return info;
  }


  public String getUsername() {
    return user;
  }

  public String getPassword() {
    return password;
  }

  public String getSchema() {
    return (schema.length() > 0 ? schema : user);
  }

  public String toString() {
    return ("/user=" + user
      + "/password=" + password
      + "/schema=" + schema);
  }

}
