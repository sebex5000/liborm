package de.sebex.orm.dbconnection;

/**
 * Created by al13555 on 22.05.2017.
 */
public enum DBType {

  ORACLE("ORACLE"),
  MYSQL("MYSQL");

  private String dbtype = null;

  DBType(String dbtype) {
    this.dbtype = dbtype;
  }



}

