package de.sebex.orm;

import java.util.HashMap;

/**
 * Created by sebastian on 12.03.17.
 */
public class UndefinedOrmObject extends BaseOrmObject {
  public UndefinedOrmObject() {}
  public UndefinedOrmObject(HashMap<String, Cell> map) {
    setCells(map);
  }
}
