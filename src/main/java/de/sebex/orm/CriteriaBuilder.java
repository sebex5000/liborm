package de.sebex.orm;


import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by al13555 on 30.03.2017.
 *
 * CriterialBuilder only builds AND-combinations
 *
 * Example:
 * <pre>
 *   {@code
 *
List<Criteria> criteriaList = new CriteriaBuilder()
  .col(FitKeyword.col_ID).equals(45)
  .getCrits();

List<Criteria> criteriaList = new CriteriaBuilder()
  .col(FitKeyword.col_ID).greaterThan(23)
  .col(FitKeyWord.col_MODIFIED_BY).lessThan(...)
  .getCrits();

 * }
 * </pre>
 *
 */
public class CriteriaBuilder {

  public static class Crit {
    private CriteriaBuilder criteriaBuilder = null;
    private String column = null;
    public Crit(CriteriaBuilder criteriaBuilder, String column) {
      this.criteriaBuilder = criteriaBuilder;
      this.column = column;
    }
    public CriteriaBuilder equals (int value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.equals, value, Types.INTEGER));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder equals (Integer value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.equals, value, Types.INTEGER));
      return this.criteriaBuilder;
    }
    public CriteriaBuilder equals (BigDecimal value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.equals, value, Types.NUMERIC));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder equals (Timestamp value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.equals, value, Types.TIMESTAMP));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder equals (Date value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.equals, value, Types.DATE));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder equals (String value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.equals, value, Types.VARCHAR));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder like (String value, boolean CaseSensitive) {
      if (CaseSensitive) {
        this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.likeCS, value, Types.VARCHAR));
      }
      else {
        this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.like, value, Types.VARCHAR));
      }

      return this.criteriaBuilder;
    }

    public CriteriaBuilder like (String value) {
      this.like(value, false);
      return this.criteriaBuilder;
    }

    public CriteriaBuilder greaterThan (int value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterThan, value, Types.INTEGER));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder greaterThan (BigDecimal value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterThan, value, Types.NUMERIC));
      return this.criteriaBuilder;
    }
    public CriteriaBuilder greaterThan (Timestamp value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterThan, value, Types.TIMESTAMP));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder greaterThan (Date value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterThan, value, Types.DATE));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder greaterEqualsThan (int value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterEqualsThan, value, Types.INTEGER));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder greaterEqualsThan (BigDecimal value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterEqualsThan, value, Types.NUMERIC));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder greaterEqualsThan (Timestamp value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterEqualsThan, value, Types.TIMESTAMP));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder greaterEqualsThan (Date value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.greaterEqualsThan, value, Types.DATE));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder lessThan (int value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessThan, value, Types.INTEGER));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder lessThan (BigDecimal value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessThan, value, Types.NUMERIC));
      return this.criteriaBuilder;
    }


    public CriteriaBuilder lessThan (Timestamp value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessThan, value, Types.TIMESTAMP));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder lessThan (Date value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessThan, value, Types.DATE));
      return this.criteriaBuilder;
    }


    public CriteriaBuilder lessEqualsThan (int value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessEqualsThan, value, Types.INTEGER));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder lessEqualsThan (BigDecimal value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessEqualsThan, value, Types.NUMERIC));
      return this.criteriaBuilder;
    }


    public CriteriaBuilder lessEqualsThan (Timestamp value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessEqualsThan, value, Types.TIMESTAMP));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder lessEqualsThan (Date value) {
      this.criteriaBuilder.addToCrits(Criteria.create(column, Criteria.Operator.lessEqualsThan, value, Types.DATE));
      return this.criteriaBuilder;
    }


    public CriteriaBuilder in (Integer[] values) {
      this.criteriaBuilder.addToCrits(Criteria.createForInOperator(column, values, Types.INTEGER));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder in (BigDecimal[] values) {
      this.criteriaBuilder.addToCrits(Criteria.createForInOperator(column, values, Types.NUMERIC));
      return this.criteriaBuilder;
    }

    public CriteriaBuilder in (String[] values) {
      this.criteriaBuilder.addToCrits(Criteria.createForInOperator(column, values, Types.VARCHAR));
      return this.criteriaBuilder;
    }

  }

  public List<Criteria> build() {
    return crits;
  }

  private List<Criteria> crits = new ArrayList<>();

  private void addToCrits(Criteria criteria) {
    this.crits.add(criteria);
  }

  public Crit col(String column) {
    return new Crit(this, column);
  }

}
