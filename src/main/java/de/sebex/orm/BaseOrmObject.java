package de.sebex.orm;

import org.gradle.internal.impldep.org.bouncycastle.util.Times;

import java.math.BigDecimal;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by sebastian on 12.03.17.
 */
public class BaseOrmObject {

  public String[] getCols() {
    return new String[] {};
  }
  public Integer[] getColTypes() {return new Integer[] {};}
  public void initColMap() {
    for(int i=0; i<getCols().length; i++) {
      colMap.put(getCols()[i], getColTypes()[i]);
    }
  }

  public BaseOrmObject() {
    initColMap();
  }
  private Map<String, Integer> colMap = new HashMap<>();

  private Map<String, Cell> cells = new HashMap<>();

  public void setCells(HashMap<String, Cell> map) {
    this.cells = map;
  }

  public Map<String, Cell> getCells() {
    return cells;
  }



  public String get(String colname) {
    Cell cell = cells.get(colname.toUpperCase());
    if (cell!=null && cell.value != null)
      return cell.value.toString();
    else return "{null}";
  }

  public void setValue(String colname, Cell cell) {
    getCells().put(colname, cell);
  }

  private Cell getOrCreateCell(String col) {
    if( getCells().containsKey(col)) {
      return getCells().get(col);
    }else {
      Cell cell = new Cell();
      cell.label = col;
      cell.name = col;
      getCells().put(col, cell);
      return cell;
    }
  }

  public void setCell(String col, Object value) {
    Cell cell = getOrCreateCell(col);
    cell.value = value;
    cell.columnType = colMap.get(col);
  }

  public Optional<BigDecimal> optionalOfBigDecimal(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(BigDecimal.valueOf(Double.valueOf(cell.value.toString())));
  }

  public BigDecimal valueOfBigDecimal(String colname) {
    Optional<BigDecimal> optional = optionalOfBigDecimal(colname);
    return optional.isPresent() ? optional.get() : null;
  }

  public void valueOfBigDecimal(String col, BigDecimal value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.NUMERIC;
    cell.value = value.toString();
  }

  public void valueOfString(String col, String value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.VARCHAR;
    cell.value = value;
  }

  public Optional<String> optionalOfString(String colname) {
      Cell cell = getCells().get(colname);
      return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(cell.value.toString());
  }

  public String valueOfString(String colname) {
    Optional<String> optional = optionalOfString(colname);
    return optional.isPresent() ? optional.get() : null;
  }

  public void valueOfTimestamp(String col, Timestamp value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.TIMESTAMP;
    cell.value = value.toString();
  }

  public Optional<Timestamp> optionalOfTimestamp(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Timestamp.valueOf(cell.value.toString()));
  }

  public Timestamp valueOfTimestamp(String colname) {
    Optional<Timestamp> optional = optionalOfTimestamp(colname);
    return optional.isPresent() ? optional.get() : null;
  }

  public void valueOfByte(String col, Byte value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.TINYINT;
    cell.value = value.toString();
  }

  public Optional<Byte> optionalOfByte(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Byte.valueOf(cell.value.toString()));
  }

  public Byte valueOfByte(String colname) {
    Optional<Byte> optional = optionalOfByte(colname);
    return optional.isPresent() ? optional.get() : null;
  }

  public void valueOfShort(String col, Short value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.SMALLINT;
    cell.value = value.toString();
  }

  public Optional<Short> optionalOfShort(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Short.valueOf(cell.value.toString()));
  }

  public Short valueOfShort(String colname) {
    Optional<Short> optional = optionalOfShort(colname);
    return optional.isPresent() ? optional.get() : null;
  }

  public void valueOfInteger(String col, Integer value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.INTEGER;
    cell.value = value.toString();
  }

  public Optional<Integer> optionalOfInteger(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Integer.valueOf(cell.value.toString()));
  }

  public Integer valueOfInteger(String colname) {
    Optional<Integer> optional = optionalOfInteger(colname);
    return optional.isPresent() ? optional.get() : null;
  }

  public void valueOfDouble(String col, Double value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.DOUBLE;
    cell.value = value.toString();
  }

  public Optional<Double> optionalOfDouble(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Double.valueOf(cell.value.toString()));
  }

  public Double valueOfDouble(String colname) {
    Optional<Double> optional = optionalOfDouble(colname);
    return optional.isPresent() ? optional.get() : null;
  }

  public void valueOfLong(String col, Long value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.BIGINT;
    cell.value = value.toString();
  }


  public Optional<Long> optionalOfLong(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Long.valueOf(cell.value.toString()));
  }

  public Long valueOfLong(String colname) {
    Optional<Long> optional = optionalOfLong(colname);
    return optional.isPresent() ? optional.get() : null;
  }


  public void valueOfBoolean(String col, Boolean value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.BOOLEAN;
    cell.value = value.toString();
  }


  public Optional<Boolean> optionalOfBoolean(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Boolean.valueOf(cell.value.toString()));
  }

  public Boolean valueOfBoolean(String colname) {
    Optional<Boolean> optional = optionalOfBoolean(colname);
    return optional.isPresent() ? optional.get() : null;
  }


  public void valueOfDate(String col, Date value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.DATE;
    cell.value = value.toString();
  }

  public Optional<Date> optionalOfDate(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(Date.valueOf(cell.value.toString()));
  }

  public Date valueOfDate(String colname) {
    Optional<Date> optional = optionalOfDate(colname);
    return optional.isPresent() ? optional.get() : null;
  }


  public void valueOfClob(String col, String value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.CLOB;
    cell.value = value;
  }

  public Optional<String> optionalOfClob(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of(cell.value.toString());
  }

  public String valueOfClob(String colname) {
    Optional<String> optional = optionalOfClob(colname);
    return optional.isPresent() ? optional.get() : null;
  }


  public void valueOfBlob(String col, byte[] value) {
    Cell cell = getOrCreateCell(col);
    cell.columnType = Types.BLOB;
    cell.value = value.toString();
  }

  public Optional<byte[]> optionalOfBlob(String colname) {
    Cell cell = getCells().get(colname);
    return (cell == null || cell.value == null) ? Optional.empty() : Optional.of((byte[]) cell.value);
  }

  public byte[] valueOfBlob(String colname) {
    Optional<byte[]> optional = optionalOfBlob(colname);
    return optional.isPresent() ? optional.get() : null;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("\t"+String.format("%-25s","=========================")+String.format("%-25s","=========================")+System.lineSeparator());
    sb.append(this.getClass().getSimpleName()+System.lineSeparator());
    sb.append("\t"+String.format("%-25s","ColumnName")+String.format("%-25s","Value")+System.lineSeparator());
    sb.append("\t"+String.format("%-25s","-------------------------")+String.format("%-25s","-------------------------")+System.lineSeparator());
    for(Cell cell : getCells().values()) {
      sb.append("\t"+String.format("%-25s",cell.name)+String.format("%-25s",cell.value)+System.lineSeparator());
    }
    sb.append("\t"+String.format("%-25s","=========================")+String.format("%-25s","=========================")+System.lineSeparator());
    return sb.toString();
  }
}
