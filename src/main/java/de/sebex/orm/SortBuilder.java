package de.sebex.orm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by al13555 on 18.04.2017.
 */
public class SortBuilder {

  public enum Order {
    ascending,
    descending
  }

  public static class Sort {
    public String Column = null;
    public Order order = Order.ascending;

    public Sort(String column, Order order) {
      this.Column = column;
      this.order = order;
    }
  }

  private List<Sort> sorts = new ArrayList<>();

  public List<Sort> getSorts() {
    return this.sorts;
  }

  public SortBuilder ascending(String column) {
    this.sorts.add(new Sort(column, Order.ascending));
    return this;
  }

  public SortBuilder descending(String column) {
    this.sorts.add(new Sort(column, Order.descending));
    return this;
  }
}
