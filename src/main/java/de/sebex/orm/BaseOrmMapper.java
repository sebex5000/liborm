package de.sebex.orm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by sebastian on 10.03.17.
 */
public class BaseOrmMapper  {

//  private static Map<Integer, Class> typemap = null;
//  private static Map<Integer, String> functionmap = null;

  static {
//    typemap = new HashMap<>();
//    typemap.put(Types.TINYINT, Byte.class);
//    typemap.put(Types.SMALLINT, Short.class);
//    typemap.put(Types.INTEGER, Integer.class);
//    typemap.put(Types.BIGINT, Long.class);
//    typemap.put(Types.FLOAT, Double.class);
//    typemap.put(Types.DOUBLE, Double.class);
//    typemap.put(Types.DECIMAL, BigDecimal.class);
//    typemap.put(Types.NUMERIC, BigDecimal.class);
//    typemap.put(Types.VARCHAR, String.class);
//    typemap.put(Types.CHAR, String.class);
//    typemap.put(Types.BIT, Boolean.class);
//    typemap.put(Types.BOOLEAN, Boolean.class);
//    typemap.put(Types.DATE, java.util.Date.class);
//    typemap.put(Types.TIMESTAMP, Timestamp.class);
//    typemap.put(Types.CLOB, Clob.class);
//    typemap.put(Types.BLOB, Blob.class);

//    functionmap = new HashMap<>();
//    functionmap.put(Types.TINYINT, "getValueOfByte");
//    functionmap.put(Types.SMALLINT, "getValueOfShort");
//    functionmap.put(Types.INTEGER, "getValueOfInteger");
//    functionmap.put(Types.BIGINT, "getValueOfLong");
//    functionmap.put(Types.FLOAT, "getValueOfDouble");
//    functionmap.put(Types.DOUBLE, "getValueOfDouble");
//    functionmap.put(Types.DECIMAL, "getValueOfBigDecimal");
//    functionmap.put(Types.NUMERIC, "getValueOfBigDecimal");
//    functionmap.put(Types.VARCHAR, "getValueOfString");
//    functionmap.put(Types.CHAR, "getValueOfString");
//    functionmap.put(Types.BIT, "getValueOfBoolean");
//    functionmap.put(Types.BOOLEAN, "getValueOfBoolean");
//    functionmap.put(Types.DATE, "getValueOfDate");
//    functionmap.put(Types.TIMESTAMP, "getValueOfTimestamp");
//    functionmap.put(Types.CLOB, "getValueOfClob");
//    functionmap.put(Types.BLOB, "getValueOfBlob");

  }

  private boolean readLobsOnSelect = false;

//  private String getMappedType(int jdbctype) {
//    if (typemap.containsKey(jdbctype)) {
//      return typemap.get(jdbctype).getSimpleName();
//    } else return String.class.getSimpleName();
//  }

//  private String getMappedFunction(int jdbctype) {
//    if (functionmap.containsKey(jdbctype)) {
//      return functionmap.get(jdbctype);
//    }
//    throw new IllegalArgumentException("Keine Methode f�r den JdbcType " + jdbctype + " gefunden.");
//  }

  private static Logger logger = LoggerFactory.getLogger(BaseOrmMapper.class);

  private Connection connection = null;

  public BaseOrmMapper() {

  }



  public Connection getConnection() {
    return connection;
  }

  public void setConnection(Connection connection) {
    this.connection = connection;
  }

  public <T extends BaseOrmObject> T newInstance(Class<T> objectclazz, String tableName) throws SQLException {

    try {
      T instance = objectclazz.newInstance();
      try (Statement statement = getConnection().createStatement()) {
        try (ResultSet rs = statement.executeQuery("select * from " + tableName + " where 1=2")) {
          instance.setCells(createRowFromResultSet(rs, false));
        }
      }
      return instance;
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  public HashMap<String, Cell> createRowFromResultSet(ResultSet rs, boolean withLobs) throws SQLException {
    HashMap<String, Cell> cells = new HashMap<>();

    List<Integer> lobtypes = new ArrayList<>();
    lobtypes.add(Types.CLOB);
    lobtypes.add(Types.BLOB);
    lobtypes.add(Types.LONGVARCHAR);

    ResultSetMetaData metaData = rs.getMetaData();

    for (int i = 1; i <= metaData.getColumnCount(); i++) {
      Cell cell = new Cell();
      cell.columnType = metaData.getColumnType(i);
      cell.name = metaData.getColumnName(i);
      cell.label = metaData.getColumnLabel(i);

      if (withLobs == false && lobtypes.contains(cell.columnType)) continue;

      switch (cell.columnType) {
        case Types.TINYINT:
        case Types.INTEGER:
        case Types.SMALLINT:
//          cell.value = rs.getString(i);
//          break;

        case Types.BIGINT:
        case Types.NUMERIC:
//          cell.value = rs.getLong(i);
//          break;
        case Types.DOUBLE:
        case Types.DECIMAL:
//          cell.value = rs.getDouble(i);
        case Types.FLOAT:
//          cell.value = rs.getFloat(i);
//          break;
        case Types.VARCHAR:
        case Types.CHAR:
//          cell.value = rs.getString(i);
//          break;
        case Types.BIT:
        case Types.BOOLEAN:
//          cell.value = rs.getBoolean(i);
        case Types.DATE:
//          cell.value = rs.getDate(i);
//          break;
        case Types.TIMESTAMP:
//          cell.value = rs.getTimestamp(i);
          cell.value = rs.getString(i);
          break;
        case Types.LONGVARCHAR:
        case Types.CLOB:
          Clob clob = rs.getClob(i);
          if (clob != null) {
            StringBuffer sb = new StringBuffer();
            try (Reader reader = clob.getCharacterStream()) {
              try (BufferedReader in = new BufferedReader(reader)) {
                char[] buf = new char[4096];
                int count = -1;
                while ((count = in.read(buf)) > -1) {
                  sb.append(buf, 0, count);
                }
                cell.value = sb.toString();
              }
            } catch (IOException e) {
              logger.error("Fehler beim Lesen der Spalte " + cell.name, e);
            }
          }
          break;
        case Types.BLOB:
          Blob blob = rs.getBlob(i);
          if (blob != null) {
            try (InputStream input = blob.getBinaryStream()) {
              byte[] b = new byte[(int) blob.length()];
              int count = input.read(b);

              cell.value = b.toString();
              if (count != blob.length())
                throw new SQLException("blob.length()-Error");

            } catch (IOException e) {
              logger.error("Fehler beim Lesen der Spalte " + cell.name, e);
            }
          }
          break;
        default:

          logger.error("Mapping Class Not Found: JDBC.Types: " + cell.columnType);
          break;
      }
      cells.put(cell.name, cell);
    }

    return cells;
  }

  /**
   * @param clazz
   * @param tableName
   * @param crits
   * @param <T>
   * @return
   * @throws SQLException
   * @deprecated use selectByCriterias instead
   */
  public <T extends BaseOrmObject> List<T> selectByCrits(Class<T> clazz, String tableName, HashMap<String, Object> crits) throws SQLException {
    List<T> list = new ArrayList<T>();
    String[] critArray = new String[crits.size()];
    Object[] critkeys = crits.keySet().toArray();
    for (int critindex = 0; critindex < crits.size(); critindex++) {
      critArray[critindex] = critkeys[critindex] + " = ?";
    }
    String whereString = String.join(" and ", critArray);

    String selectSQL = "select * from " + tableName + " where " + whereString;
    try (PreparedStatement preparedStatement = getConnection().prepareStatement(selectSQL)) {
      for (int critindex = 0; critindex < critkeys.length; critindex++) {
        int jdbccolindex = critindex + 1;
        preparedStatement.setObject(jdbccolindex, crits.get(critkeys[critindex]));
      }
      try (ResultSet rs = preparedStatement.executeQuery()) {

        while (rs.next()) {
          T object = clazz.newInstance();
          object.setCells(createRowFromResultSet(rs, true));
          list.add(object);
        }
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InstantiationException e) {
        e.printStackTrace();
      }
    }
    return list;
  }


  public <T extends BaseOrmObject> void deleteByCrits(Class<T> clazz, String tableName, HashMap<String, Object> crits) throws SQLException {
    List<T> list = new ArrayList<T>();
    String[] critArray = new String[crits.size()];
    Object[] critkeys = crits.keySet().toArray();
    for (int critindex = 0; critindex < crits.size(); critindex++) {
      critArray[critindex] = critkeys[critindex] + " = ?";
    }
    String whereString = String.join(" and ", critArray);

    String selectSQL = "delete from " + tableName + " where " + whereString;
    try (PreparedStatement preparedStatement = getConnection().prepareStatement(selectSQL)) {
      for (int critindex = 0; critindex < critkeys.length; critindex++) {
        int jdbccolindex = critindex + 1;
        preparedStatement.setObject(jdbccolindex, crits.get(critkeys[critindex]));
      }
      preparedStatement.execute();
    }
  }


  public <T extends BaseOrmObject> Stream<T> selectByCriteriasStream(Class<T> targetObjectClass, String tableName, List<Criteria> crits) throws SQLException {
    List<T> list = new ArrayList<T>();

    PreparedStatement preparedStatement = getPreparedStatement(tableName, crits);
    ResultSet rs = preparedStatement.executeQuery();

    return StreamSupport
      .stream(Spliterators.spliteratorUnknownSize(
        new ResultSetIterator(targetObjectClass,this, rs, preparedStatement), 0), false);

  }

  private PreparedStatement getPreparedStatement(String tableName, List<Criteria> crits) throws SQLException {
    String[] critArray = new String[0];
    if (crits != null) {
      critArray = new String[crits.size()];

      for (int critindex = 0; critindex < crits.size(); critindex++) {
        Criteria criteria = crits.get(critindex);
        switch (criteria.getOperator()) {
          case equals:
            critArray[critindex] = criteria.getColumn() + " = ?";
            break;
          case like:
            critArray[critindex] = "upper(" + criteria.getColumn() + ") like ?";
            break;
          case likeCS:
            critArray[critindex] = criteria.getColumn() + " like ?";
            break;
          case greaterEqualsThan:
            critArray[critindex] = criteria.getColumn() + " >= ?";
            break;
          case greaterThan:
            critArray[critindex] = criteria.getColumn() + " > ?";
            break;
          case lessThan:
            critArray[critindex] = criteria.getColumn() + " < ?";
            break;
          case lessEqualsThan:
            critArray[critindex] = criteria.getColumn() + " <= ?";
            break;
          case in:
//IN-Operator is experimental and does not work with Preparedstatements
            Object[] paramvalues = (Object[]) criteria.getValue();
            String[] params = new String[paramvalues.length];

//          Arrays.fill(params, "?");
            for (int index = 0; index < params.length; index++) {
              params[index] = "'" + paramvalues[index] + "'";
            }

            String strvalue = String.join(", ", params);
            critArray[critindex] = criteria.getColumn() + " in (" + strvalue + ")";

            break;
          default:
            break;
        }
      }
    }
    String whereString = "";
    if (crits != null && crits.size() > 0) whereString = " where " + String.join(" and ", critArray);

    String selectSQL = "select * from " + tableName + whereString;
    PreparedStatement preparedStatement = getConnection().prepareStatement(selectSQL);
    int jdbccolindex = 0;
    for (int critindex = 0; critindex < critArray.length; critindex++) {
      Criteria criteria = crits.get(critindex);

      if (criteria.getOperator() == Criteria.Operator.in) {
//IN-Operator is experimental and does not work with Preparedstatements
//          Object[] paramvalues = (Object[]) criteria.getValue();
//
//          for(int index = 0; index<paramvalues.length; index++) {
//            preparedStatement.setObject(++jdbccolindex, "'"+paramvalues[index]+"'");
//          }

      } else {
        if (criteria.getOperator() == Criteria.Operator.like) {
          preparedStatement.setString(++jdbccolindex, criteria.getValue().toString().toUpperCase());
        } else {

          switch (criteria.getJdbcType()) {
            case Types.TIMESTAMP:
              preparedStatement.setTimestamp(++jdbccolindex, (Timestamp) criteria.getValue());
              break;
            case Types.DATE:
              preparedStatement.setDate(++jdbccolindex, (Date) criteria.getValue());
              break;
            default:
              preparedStatement.setObject(++jdbccolindex, criteria.getValue());
              break;
          }

        }
      }
    }
    return preparedStatement;
  }


  /**
   * alias for selectByCriterias
   *
   * @param clazz
   * @param tableName
   * @param crits
   * @return typeOf BaseOrmObject
   */
  public <T extends BaseOrmObject> List<T> select(Class<T> clazz, String tableName, List<Criteria> crits) {
    return selectByCriterias(clazz, tableName, crits);
  }

  /**
   * @param clazz     Mapperclass with SQL-Methods
   * @param tableName
   * @param crits
   * @return typeOf BaseOrmObject
   */
  public <T extends BaseOrmObject> List<T> selectByCriterias(Class<T> clazz, String tableName, List<Criteria> crits) {
    List<T> list = new ArrayList<T>();

    try (PreparedStatement preparedStatement = getPreparedStatement(tableName, crits)) {
      try (ResultSet rs = preparedStatement.executeQuery()) {
        while (rs.next()) {
          T object = clazz.newInstance();
          object.setCells(createRowFromResultSet(rs, isReadLobsOnSelect()));
          list.add(object);
        }
      }
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
    return list;
  }

  public List<BaseOrmObject> selectByQuery(String query, boolean withLobs) {
    return getListFromQuery(BaseOrmObject.class, query, withLobs);
  }

  public <T extends BaseOrmObject> List<T> selectByQuery(Class<T> targetObjectClass, String query) {
    return getListFromQuery(targetObjectClass, query, isReadLobsOnSelect());
  }

  public <T extends BaseOrmObject> Stream<T> selectByQueryAsStream(Class<T> targetObjectClass, String query) {
    try {
      Statement stmt = getConnection().createStatement();
      ResultSet rs = stmt.executeQuery(query);

      return StreamSupport
        .stream(Spliterators.spliteratorUnknownSize(
          new ResultSetIterator(targetObjectClass, this, rs, stmt), 0), false);

    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public <T extends BaseOrmObject> void insert(String tableName, String[] cols, List<T> list) throws SQLException {
    String colstring = String.join(", ", cols);
    String[] values = new String[cols.length];
    Arrays.fill(values, "?");
    String valueString = String.join(", ", values);
    String insertSQL = "insert into " + tableName + " (" + colstring + ") values (" + valueString + ")";
    try (PreparedStatement preparedStatement = getConnection().prepareStatement(insertSQL)) {

      for (int listindex = 0; listindex < list.size(); listindex++) {
        T row = list.get(listindex);
        for (int colindex = 0; colindex < cols.length; colindex++) {
          int jdbccolindex = colindex + 1;
          Cell cell = row.getCells().get(cols[colindex]);
          fillPreparedStatement(cell, preparedStatement, jdbccolindex);
        }
        preparedStatement.execute();
      }
    }
  }

  public <T extends BaseOrmObject> void update(String tableName, String[] cols, String[] keyCols, List<T> list) throws SQLException {

    if (keyCols.length == 0) {
      throw new SQLException("Keine Primarykeys angegeben.");
    }

    StringBuilder updateSql = new StringBuilder();
    updateSql.append("update " + tableName + " set ");

    String[] setvalues = new String[cols.length];
    for (int cellindex = 0; cellindex < cols.length; cellindex++) {
      setvalues[cellindex] = cols[cellindex] + " = ? ";
    }
    updateSql.append(String.join(", ", setvalues));
    updateSql.append(" where ");

    String[] wherevalues = new String[keyCols.length];
    for (int colindex = 0; colindex < keyCols.length; colindex++) {
      wherevalues[colindex] = keyCols[colindex] + " = ? ";
    }
    updateSql.append(String.join(" and ", wherevalues));
    PreparedStatement preparedStatement = getConnection().prepareStatement(updateSql.toString());

    for (int listindex = 0; listindex < list.size(); listindex++) {
      T row = list.get(listindex);

      int jdbcparamindex = 0;
      for (int colindex = 0; colindex < cols.length; colindex++) {
        Cell cell = row.getCells().get(cols[colindex]);
        fillPreparedStatement(cell, preparedStatement, ++jdbcparamindex);
      }
      for (int whereindex = 0; whereindex < keyCols.length; whereindex++) {
        Cell cell = row.getCells().get(keyCols[whereindex]);
        fillPreparedStatement(cell, preparedStatement, ++jdbcparamindex);
      }

      preparedStatement.execute();
    }
  }


  private <T extends BaseOrmObject> void fillPreparedStatement(Cell cell1, PreparedStatement preparedStatement, int jdbccolindex) throws SQLException {

    Cell cell = cell1;
    if (cell == null) {
      preparedStatement.setObject(jdbccolindex, null);
      return;
    }

    if (cell.value == null) {
      preparedStatement.setNull(jdbccolindex, cell.columnType);
      return;
    }

    switch (cell.columnType) {
      case Types.VARCHAR:
      case Types.CHAR:
        preparedStatement.setString(jdbccolindex, cell.value.toString());
        break;
      case Types.TINYINT:
      case Types.INTEGER:
      case Types.SMALLINT:
        preparedStatement.setInt(jdbccolindex, Integer.valueOf(cell.value.toString()));
        break;
      case Types.DECIMAL:
      case Types.BIGINT:
      case Types.NUMERIC:
      case Types.DOUBLE:
        preparedStatement.setDouble(jdbccolindex, Double.valueOf(cell.value.toString()));
        break;
      case Types.FLOAT:
        preparedStatement.setFloat(jdbccolindex, Float.valueOf(cell.value.toString()));
        break;
      case Types.BIT:
      case Types.BOOLEAN:
        preparedStatement.setBoolean(jdbccolindex, Boolean.valueOf(cell.value.toString()));
        break;
      case Types.DATE:
        preparedStatement.setDate(jdbccolindex, Date.valueOf(cell.value.toString()));
        break;
      case Types.TIMESTAMP:
        preparedStatement.setTimestamp(jdbccolindex, Timestamp.valueOf(cell.value.toString()));
        break;
      case Types.CLOB:
        Clob clob = getConnection().createClob();
        clob.setString(1, cell.value.toString());
        preparedStatement.setClob(jdbccolindex, clob);
        break;
      case Types.BLOB:
        Blob blob = getConnection().createBlob();
        blob.setBytes(1, (byte[]) cell.value.toString().getBytes());
        preparedStatement.setBlob(jdbccolindex, blob);
        break;
      default:
        logger.error("Mapping Class Not Found: JDBC.Types: " + cell.columnType);
        break;

    }
  }


  protected <T extends BaseOrmObject> List<T> getListFromQuery(Class<T> targetObject, String query, boolean withLobs) {
    List<T> list = new ArrayList<T>();
    try (Statement stmt = getConnection().createStatement()) {
      try (ResultSet rs = stmt.executeQuery(query)) {
        while (rs.next()) {
          T object = targetObject.newInstance();
          object.setCells(createRowFromResultSet(rs, withLobs));
          list.add(object);
        }
        return list;

      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InstantiationException e) {
        e.printStackTrace();
      }
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
    return null;
  }




  public boolean isReadLobsOnSelect() {
    return readLobsOnSelect;
  }

  public void setReadLobsOnSelect(boolean readLobsOnSelect) {
    this.readLobsOnSelect = readLobsOnSelect;
  }
}
