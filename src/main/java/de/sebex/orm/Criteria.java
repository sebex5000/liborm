package de.sebex.orm;


/**
 * Created by al13555 on 30.03.2017.
 */
public class Criteria {

  public String getColumn() {
    return column;
  }

  public void setColumn(String column) {
    this.column = column;
  }

  /**
   * like = like with CaseInSencitive default
   * likeCS = like with Casesensitive
   */
  public static enum Operator {
    equals,
    like,
    likeCS,
    greaterThan,
    greaterEqualsThan,
    lessThan,
    lessEqualsThan,
    in
  }

  private Operator operator = Operator.equals;
  private String column = null;
  private Object[] values = null;
  private int jdbcType = 0;

  public Criteria() { }

  public static Criteria create(String column, Operator operator, Object object, int jdbcType) {
    Criteria newCriteria = new Criteria();
    newCriteria.setColumn(column);
    newCriteria.setOperator(operator);
    newCriteria.setValue(object);
    newCriteria.setJdbcType(jdbcType);
    return newCriteria;
  }

  public static Criteria createForInOperator(String column, Object[] objects, int jdbcType) {
    Criteria newCriteria = new Criteria();
    newCriteria.setColumn(column);
    newCriteria.setOperator(Operator.in);
    newCriteria.setValue(objects);
    newCriteria.setJdbcType(jdbcType);
    return newCriteria;
  }

  public Operator getOperator() {
    return operator;
  }

  public void setOperator(Operator operator) {
    this.operator = operator;
  }

  public Object getValue() {
    if (this.values.length > 0)
      return values[0];
    else return null;
  }


  public void setValue(Object value) {
    this.values = new Object[1];
    this.values[0] = value;
  }

  public int getJdbcType() {
    return jdbcType;
  }

  public void setJdbcType(int jdbcType) {
    this.jdbcType = jdbcType;
  }


}
